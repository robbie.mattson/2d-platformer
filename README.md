# 2D Platformer

Initially, I wanted to learn how to create the mechanics of a 2D platforming videogame, similar to Mario bros.
In Unity I created a 2D platformer that is similar to said title. You can move left and right, jump, and collect
coins. I learned quite a bit about event listeners, collisions, and scenes. 